using System;
using System.Collections.Generic;
using System.Linq;

namespace NotificationRecover
{
  internal class Program
  {
    private static void Main(string[] args)
    {
      if (args.Length == 0)
      {
        Program.DisplayUsage();
      }
      else
      {
        try
        {
          Program.Parse(args).Execute();
        }
        catch (Exception ex)
        {
          Program.DisplayError(ex.Message);
        }
      }
    }

    private static void DisplayError(string message)
    {
      Console.Out.WriteLine(string.Format("Error : {0}", (object) message));
      Console.Out.WriteLine("");
      Program.DisplayUsage();
    }

    private static Command Parse(string[] args)
    {
      Command command = new Command(Console.Out);
      command.NotificationFolder = args[0];
      List<string> list = ((IEnumerable<string>) args).ToList<string>();
      list.RemoveAt(0);
      List<KeyValuePair<string, string>> keyValuePairList = new List<KeyValuePair<string, string>>();
      int index = 0;
      while (index < list.Count)
      {
        keyValuePairList.Add(new KeyValuePair<string, string>(list[index], list[index + 1]));
        if (!list[index].StartsWith("-"))
          Program.DisplaySyntaxError();
        if (list[index + 1].StartsWith("-"))
          Program.DisplaySyntaxError();
        if (list[index] == "-l")
          command.LanguageCode = list[index + 1];
        else if (list[index] == "-s")
        {
          if (list[index + 1] == "global")
          {
            command.SubscriptionId = "global";
          }
          else
          {
            int result;
            if (int.TryParse(list[index + 1], out result))
              command.SubscriptionId = list[index + 1];
            else
              Program.DisplaySyntaxError();
          }
        }
        else if (list[index] == "-n")
          command.TemplateName = list[index + 1];
        index += 2;
      }
      return command;
    }

    private static void DisplaySyntaxError()
    {
      Console.Out.WriteLine("Syntax error.");
      Console.Out.WriteLine("");
      Program.DisplayUsage();
    }

    private static void DisplayUsage()
    {
      Console.Out.WriteLine("Usage: notificationRecover <notificationFolderPath>");
      Console.Out.WriteLine("");
      Console.Out.WriteLine("où <notificationFolderPath> est le chemin du dossier racine où seront téléchargées toutes les notifications.");
      Console.Out.WriteLine("");
      Console.Out.WriteLine("Options:");
      Console.Out.WriteLine("-l <languageCode>           où <languageCode> est le code de la langue au format 'fr-FR'");
      Console.Out.WriteLine("-s <subscriptionId>         où <subscriptionId> est l'id de la souscription pour laquelle on upload les notifications ou 'global'");
      Console.Out.WriteLine("-n <templateName>           où <templateName> est le nom du template à télécharger");
      Console.Out.WriteLine("");
      Console.Out.WriteLine("Exemple:");
      Console.Out.WriteLine("notificationRecover 'c:\\notifications' -l fr-FR -s 15");
    }
  }
}
