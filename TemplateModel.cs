﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NotificationRecover
{
    public class TemplateModel
    {
        public string name { get; set; }

        public string culture { get; set; }

        public int? subscription { get; set; }
    }
}
