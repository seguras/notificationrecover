using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Net.Http;
using Newtonsoft.Json;

namespace NotificationRecover
{
  public class Command
  {
    private HttpClient httpClient;

    public string WSUrl
    {
      get
      {
        return ConfigurationManager.AppSettings["WSUrl"];
      }
    }

        public string[] Languages
        {
            get
            {
                return ConfigurationManager.AppSettings["Languages"].Split(new string[] { "," }, StringSplitOptions.RemoveEmptyEntries);
            }
        }

        private HttpClient HttpClient
    {
      get
      {
        if (this.httpClient == null)
        {
          this.httpClient = new HttpClient();
          this.httpClient.BaseAddress = new Uri(this.WSUrl);
        }
        return this.httpClient;
      }
    }

    public string NotificationFolder { get; set; }

    public string LanguageCode { get; set; }

    public string TemplateName { get; set; }

    public string SubscriptionId { get; set; }

    public TextWriter Logger { get; set; }

    public Command(TextWriter logger)
    {
      this.Logger = logger;
    }

    public void Execute()
    {
      if (!new DirectoryInfo(this.NotificationFolder).Exists)
        throw new Exception(string.Format("Le dossier '{0}' n'existe pas", (object) this.NotificationFolder));
      if (string.IsNullOrEmpty(this.LanguageCode) && string.IsNullOrEmpty(this.SubscriptionId) && string.IsNullOrEmpty(this.TemplateName))
        this.ExecuteAll();
      else
        this.Execute(this.LanguageCode, this.SubscriptionId, this.TemplateName);
    }

    private void ExecuteAll()
    {
      foreach (string language in Languages)
        this.Execute(language, null, null);
    }

    //public void ExecuteForLanguage(string languageCode)
    //{
    //        if (!new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode)).Exists)
    //            Directory.CreateDirectory(Path.Combine(this.NotificationFolder, languageCode));
    //  this.Logger.WriteLine(string.Format("Process language {0}", (object) languageCode));
    //  if (string.IsNullOrEmpty(this.SubscriptionId))
    //    this.ExecuteAllSubscription(languageCode);
    //  else
    //    this.ExecuteForSubscription(languageCode, this.SubscriptionId);
    //}

    //private void ExecuteAllSubscription(string languageCode)
    //{
    //  foreach (DirectoryInfo directory in new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode)).GetDirectories())
    //    this.ExecuteForSubscription(languageCode, directory.Name);
    //}

    //public void ExecuteForSubscription(string languageCode, string SubscriptionId)
    //{
    //  if (!new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode, SubscriptionId)).Exists)
    //    throw new Exception(string.Format("Le dossier pour la souscription '{0}' dans la langue '{1}' n'existe pas", (object) SubscriptionId, (object) languageCode));
    //  this.Logger.WriteLine(string.Format("  Process subscription {0}", (object) SubscriptionId));
    //  if (string.IsNullOrEmpty(this.TemplateName))
    //    this.ExecuteAllTemplates(languageCode, SubscriptionId);
    //  else
    //    this.ExecuteForTemplate(languageCode, SubscriptionId, string.Format("{0}.eml", (object) this.TemplateName));
    //}

    //public void ExecuteForTemplate(string languageCode, string SubscriptionId, string templateName)
    //{
    //  FileInfo fileInfo = new FileInfo(Path.Combine(this.NotificationFolder, languageCode, SubscriptionId, templateName));
    //  if (!fileInfo.Exists)
    //    throw new Exception(string.Format("Le template '{0}' pour la souscription '{1}' dans la langue '{2}' n'existe pas", (object) templateName, (object) SubscriptionId, (object) languageCode));
    //  this.Logger.WriteLine(string.Format("    Process template {0}", (object) templateName));
    //  string requestUri = string.Format("{0}/{1}", (object) Path.GetFileNameWithoutExtension(templateName), (object) languageCode);
    //  if (SubscriptionId != "global")
    //    requestUri += string.Format("/{0}", (object) SubscriptionId);
    //  using (FileStream fileStream = File.OpenRead(fileInfo.FullName))
    //  {
    //    HttpClient httpClient = new HttpClient();
    //    using (MultipartFormDataContent multipartFormDataContent = new MultipartFormDataContent("Upload----" + DateTime.Now.ToString((IFormatProvider) CultureInfo.InvariantCulture)))
    //    {
    //      multipartFormDataContent.Add((HttpContent) new StreamContent((Stream) fileStream), "content", templateName);
    //      HttpResponseMessage result = this.HttpClient.PutAsync(requestUri, (HttpContent) multipartFormDataContent).Result;
    //      if (result.IsSuccessStatusCode)
    //        return;
    //      this.Logger.WriteLine(string.Format("Error: {0} {1}", (object) result.StatusCode, (object) result.ReasonPhrase));
    //    }
    //  }
    //}

    //public void ExecuteAllTemplates(string languageCode, string SubscriptionId)
    //{
    //  foreach (FileInfo file in new DirectoryInfo(Path.Combine(this.NotificationFolder, languageCode, SubscriptionId)).GetFiles("*.eml"))
    //    this.ExecuteForTemplate(languageCode, SubscriptionId, file.Name);
    //}

        public void Execute(string languageCode, string subscriptionId, string templateName)
        {
            var templates = GetTemplates(languageCode, subscriptionId, templateName);

            foreach(var template in templates)
            {
                DownloadTemplate(template);
            }
        }

        private List<TemplateModel> GetTemplates(string languageCode, string subscriptionId, string templateName)
        {
            string url = $"{WSUrl}search?";
            string param = string.Empty;

            if(!string.IsNullOrEmpty(languageCode))
            {
                if (param.Length != 0)
                {
                    param += "&";
                }

                param += $"Culture={languageCode}";
            }

            if (!string.IsNullOrEmpty(subscriptionId))
            {
                if (param.Length != 0)
                {
                    param += "&";
                }

                param += $"Subscription={subscriptionId}";
            }

            if (!string.IsNullOrEmpty(templateName))
            {
                if (param.Length != 0)
                {
                    param += "&";
                }

                param += $"Name={templateName}";
            }

            using (var client = new HttpClient())
            {
                var response = client.GetAsync($"{url}{param}").Result;

                if(response.IsSuccessStatusCode)
                {
                    var content = response.Content.ReadAsStringAsync().Result;
                    return JsonConvert.DeserializeObject<List<TemplateModel>>(content);
                }
                else
                {
                    this.Logger.WriteLine($"Erreur - {response.StatusCode} - {response.ReasonPhrase}");
                    throw new Exception("Termin�e en erreur");
                }
            }
        }

        private void DownloadTemplate(TemplateModel template)
        {
            this.Logger.WriteLine($"Process language : {template.culture} - subscription : {template.subscription} - template : {template.name}");

            string url = $"{WSUrl}{template.name}/{template.culture}/";

            if (template.subscription != null)
            {
                url += $"{template.subscription}/";
            }

            url += "content";

            string path = Path.Combine(NotificationFolder, template.culture, template.subscription != null ? template.subscription.ToString() : "global");

            if (!Directory.Exists(path))
            {
                Directory.CreateDirectory(path);
            }

            using (var client = new HttpClient())
            {
                var response = client.GetAsync(url).Result;
                if (response.IsSuccessStatusCode)
                {
                    response.Content.ReadAsFileAsync($"{Path.Combine(path, template.name)}.eml", true);
                }
                else
                {
                    this.Logger.WriteLine($"Erreur - {response.StatusCode} - {response.ReasonPhrase}");
                }
            }
        }
  }
}
